package main

import (
	"errors"
	"fmt"
	"strings"

	"gitlab.com/sgolub/advent-of-code/base/v2/days"
)

type myDay struct {
	data map[string]string
}

func (d myDay) SetData(key, value string) {
	d.data[key] = value
}

// Year will return the AOC Year
func (d myDay) Year() int {
	return 2020
}

// Day will return the day number for this puzzle
func (d myDay) Day() int {
	return 3
}

type hillMap struct {
	Map []string
}

func (m hillMap) String() string {
	return strings.Join(m.Map, "\n")
}

const (
	tree  = '#'
	empty = '.'
)

var errBelowBottom = errors.New("value is below bottom")

func (m hillMap) TreeAt(y, x int) (bool, error) {
	if y >= len(m.Map) {
		return false, errBelowBottom
	}
	x = x % len(m.Map[0])
	return m.Map[y][x] == tree, nil
}

func (m hillMap) TreeCount(down, right int) int {
	var (
		x int
		y int
		c int
	)
	for {
		a, err := m.TreeAt(y, x)
		if err == errBelowBottom {
			break
		}
		if a {
			c++
		}
		x += right
		y += down
	}
	return c
}

// GetData will load the data and parse it from disk
func (d myDay) GetData(useSampleData bool) hillMap {
	result := hillMap{
		Map: []string{},
	}
	data, ok := days.Control().LoadData(d, useSampleData)["map"]
	if !ok {
		return result
	}
	for _, row := range data.([]interface{}) {
		result.Map = append(result.Map, row.(string))
	}
	return result
}

// Solution1 is the solution to the first part of the puzzle
func (d myDay) Solution1(useSample bool) string {
	data := d.GetData(useSample)
	defer days.NewTimer("D3P1")
	return fmt.Sprint(data.TreeCount(1, 3))
}

// Solution2 is the solution to the second part of the puzzle
func (d myDay) Solution2(useSample bool) string {
	data := d.GetData(useSample)
	defer days.NewTimer("D3P2")
	result := 1
	checkSlopes := [][]int{
		{1, 1},
		{3, 1},
		{5, 1},
		{7, 1},
		{1, 2},
	}
	for _, slopes := range checkSlopes {
		result *= data.TreeCount(slopes[1], slopes[0])
	}
	return fmt.Sprint(result)
}

// GetDay will return this module's day. Used by the plugin loader.
func GetDay() days.Day {
	return myDay{
		data: map[string]string{},
	}
}
